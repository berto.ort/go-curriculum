## Part 5 - Persistence

To access SQL databases in Go, we use [database/sql](https://golang.org/pkg/database/sql) package.

For extensions and other tools, checkout [Awesome-Go](https://awesome-go.com/#database).

## Other Resources

- [Go Database Tutorial](http://go-database-sql.org/)

## Database Connection

To use `database/sql` we'll need the package itself, as well as a driver for the specific database we want to use.

We will be using a [postgresql](https://www.postgresql.org/) driver [pq](https://github.com/lib/pq)

To install run `go get github.com/lib/pq`

To import add `	_ "github.com/lib/pq"`. Notice the `_`. This means we won't be using the package directly, but the `database/sql` defined types.

To access the DB, run:

```go
connStr := "user=pqgotest dbname=pqgotest sslmode=verify-full"
// or a connection URL
// connStr := "postgres://pqgotest:password@localhost/pqgotest?sslmode=verify-full"
db, err := sql.Open("postgres", connStr)
if err != nil {
    log.Fatal(err)
}
defer db.Close()
```

This creates a [db](https://golang.org/pkg/database/sql/#DB) handle that will allow us to open and close connections to the underlying database. We will also use it to create statements and transactions.

Always check the `err` and `defer` the closing of the connection.

## Fetching Data

Examples can be found in [pq's documentation](https://godoc.org/github.com/lib/pq#hdr-Queries):

Querying rows:

```go
rows, err := db.Query(`SELECT name FROM users WHERE favorite_fruit = $1
	OR age BETWEEN $2 AND $2 + 3`, "orange", 64)
```

Creating a transaction:

```go
tx, err := db.Begin()
if err != nil {
    return err
}

defer func() {
    switch err {
    case nil:
        err = tx.Commit()
    default:
        tx.Rollback()
    }
}()

if _, err = tx.Exec("INSERT INTO product_viewers (user_id, product_id) VALUES ($1, $2)", userID, productID); err != nil {
    return err
}
return nil
```

Alternatively, use [sqlx](https://github.com/jmoiron/sqlx) to extend the standard `database/sql` library.

## Database Testing

[go-sqlmock](https://github.com/DATA-DOG/go-sqlmock) makes it easy to mock the behavior of our sql driver.

```go
db, mock, err := sqlmock.New()
if err != nil {
    t.Fatalf("an error '%s' was not expected when opening a stub database connection", err)
}
defer db.Close()

mock.ExpectBegin()
mock.ExpectExec("INSERT INTO product_viewers").WithArgs(2, 3).WillReturnResult(sqlmock.NewResult(1, 1))
mock.ExpectCommit()

// now we execute our method
if err = recordStats(db, 2, 3); err != nil {
    t.Errorf("error was not expected while updating stats: %s", err)
}

// we make sure that all expectations were met
if err := mock.ExpectationsWereMet(); err != nil {
    t.Errorf("there were unfulfilled expectations: %s", err)
}
```

## Migrations

There are libraries to manage migrations using go. For our purposes, we will be using a [CLI tool](https://github.com/golang-migrate/migrate) written in go, but writing our queries in sql.

[Installing](https://github.com/golang-migrate/migrate/tree/master/cli#with-go-toolchain):

```
go get -u -d github.com/golang-migrate/migrate/cli
cd $GOPATH/src/github.com/golang-migrate/migrate/cli
dep ensure
go build -tags 'postgres' -o /usr/local/bin/migrate github.com/golang-migrate/migrate/cli
```

[Usage](https://github.com/golang-migrate/migrate/tree/master/cli#usage):

```
mkdir migrations
migrate create -ext sql -dir ./migrations create_user
# edit up and down files
migrate -source file://./migrations -database postgres://localhost:5432/database up
```

## ORM - Stretch

Use a [Go ORM](https://github.com/jinzhu/gorm).

## Exercise

Complete the stories below:

- As a command line gamer I want to start boggle via the terminal
- As a boggle player I want to see the instructions for the game so that I know how to play
- As a boggle player I want to be able to see a boggle board so that I can play the game
- As a boggle player I want to be able to enter words for one minute in order to achieve a score
- As a boggle player that has entered words I want to be able to see my score so I can know how well I did
- As a boggle player I want to see the highest score for a single round so I can reflect on how awesome I am
- As a boggle player that has finished the game I would like to see all possible words for that game so that I can see the words I missed
- As a boggle player that has finished a game I want to be able to play another game
- As a boggle player I want to be able to save a round so that I can return to it later