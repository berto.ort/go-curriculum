package slices

import (
	"reflect"
	"testing"
)

func TestSlices(t *testing.T) {
	numbers := [5]int{1, 2, 3, 4, 5}

	got := Sum(numbers)
	want := 15

	if want != got {
		t.Errorf("got %d want %d given, %v", got, want, numbers)
	}

	users := []User{
		{false},
		{true},
		{true},
		{false},
	}

	got = FilterAdmin(users)
	wantedAdmins := []User{
		{true},
		{true},
	}

	if !reflect.DeepEqual(got, want) {
		t.Errorf("got %d want %v", got, wantedAdmins)
	}

	got = SumAll([]int{1, 2}, []int{0, 9})
	wantedSum := []int{3, 9}

	if !reflect.DeepEqual(got, want) {
		t.Errorf("got %v want %v", got, wantedSum)
	}
}
