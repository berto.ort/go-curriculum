package types

import "testing"

func TestTypes(t *testing.T) {
	assertCorrectMessage := func(t *testing.T, got, want interface{}) {
		t.Helper()
		if got != want {
			t.Errorf("got '%v' want '%v'", got, want)
		}
	}

	t.Run("doing sum calculations", func(t *testing.T) {
		got := Add(3, 2)
		want := 5
		assertCorrectMessage(t, got, want)
	})

	t.Run("doing 15% of a total", func(t *testing.T) {
		got := Percentage(85)
		want := 12.75
		assertCorrectMessage(t, got, want)
	})

	t.Run("saying hello in different languages", func(t *testing.T) {
		got := Hello("Elodie", spanish)
		want := "Hola, Elodie"
		assertCorrectMessage(t, got, want)

		got = Hello("Elodie", french)
		want = "Bonjour, Elodie"
		assertCorrectMessage(t, got, want)
	})
}
