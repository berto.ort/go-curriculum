package embedding

import "testing"

func TestEmbedding(t *testing.T) {
	teamName := "Panthers"
	teamCity := "Miami"
	country := "USA"

	sport := NewSportTeam(teamName, teamCity, country)

	if sport.Country != country {
		t.Errorf("Sport team should have a country property of '%s' not '%s'", country, sport.Country)
	}

	if sport.Team.Name != teamName {
		t.Errorf("Sport team name should be '%s' not '%s'", teamName, sport.Team.Name)
	}

	if sport.Team.City != teamCity {
		t.Errorf("Sport team city should be '%s' not '%s'", teamCity, sport.Team.City)
	}
}
