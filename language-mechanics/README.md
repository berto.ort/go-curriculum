# Part 1 - Language Mechanics

## Material

- [ ] [Language Introduction](#language-introduction) 
- [ ] [Types](#types---variables-and-constants) - [Exercise](./types)
- [ ] [Structs](#structs)- [Exercise](./structs)
- [ ] [Functions](#functions) - [Exercise](./functions)
- [ ] [Pointers and Methods](#pointers-and-methods) - [Exercise](./pointers)
- [ ] [Embedding](#embedding) - [Exercise](./embedding)
- [ ] [Arrays and Slices](#arrays-and-slices) - [Exercise](./slices)
- [ ] [Maps](#maps) - [Exercise](./maps)
- [ ] [Interfaces](#interfaces) - [Exercise](./interfaces)
- [ ] [Design Philosophy - Interfaces and Composition](#design-philosophy---interfaces-and-composition)

## Language Introduction

Go is a general-purpose language designed with systems programming in mind. It is strongly typed and garbage-collected and has explicit support for concurrent programming. Programs are constructed from packages, whose properties allow efficient management of dependencies.

- [Installation and Setup](https://github.com/quii/learn-go-with-tests/blob/master/install-go.md#install-go-set-up-environment-for-productivity)
- [Golang Spec](https://golang.org/ref/spec)
- <a href="https://en.wikipedia.org/wiki/Go_(programming_language)">Wiki</a>

## Types - variables and constants

**Basic Types**

```go
bool

string

int  int8  int16  int32  int64
uint uint8 uint16 uint32 uint64 uintptr

byte // alias for uint8

rune // alias for int32
     // represents a Unicode code point

float32 float64

complex64 complex128
```

Variables declared without an explicit initial value are given their zero value.

The zero value is:

0 for numeric types,
false for the boolean type, and
"" (the empty string) for strings.

The expression T(v) converts the value v to the type T.

```go
var i int = 42
var f float64 = float64(i)
var u uint = uint(f)
```

**Variables**

The `var` statement declares a list of variables; as in function argument lists, the type is last.

A `var` statement can be at package or function level. 

```go
var c, python, java bool

func main() {
	var i int
	fmt.Println(i, c, python, java)
}
```

A var declaration can include initializers, one per variable.

If an initializer is present, the type can be omitted; the variable will take the type of the initializer.

```go
var i, j int = 1, 2

func main() {
	var c, python, java = true, false, "no!"
	fmt.Println(i, j, c, python, java)
}
```

Inside a function, the `:=` short assignment statement can be used in place of a var declaration with implicit type.

Outside a function, every statement begins with a keyword (var, func, and so on) and so the `:=` construct is not available.

```go
func main() {
	var i, j int = 1, 2
	k := 3
	c, python, java := true, false, "no!"

	fmt.Println(i, j, k, c, python, java)
}
```

**Constants**

Constants are declared like variables, but with the const keyword.

Constants can be character, string, boolean, or numeric values.

Constants cannot be declared using the := syntax.

Numeric constants have no type until it's given one, such as by an explicit cast.

An untyped constant takes the type needed by its context.

- [Go Training - Variables](https://ultimate-go-copy.herokuapp.com/topics/go/language/variables/README.md)
- [Exercise](./types)

## Structs

A `struct` is a collection of fields.

Struct fields are accessed using a dot.

A struct literal denotes a newly allocated struct value by listing the values of its fields.

```go
type Vertex struct {
	X, Y int
}

var (
	v1 = Vertex{1, 2}  // has type Vertex
	v2 = Vertex{X: 1}  // Y:0 is implicit
	v3 = Vertex{}      // X:0 and Y:0
	p  = &Vertex{1, 2} // has type *Vertex
)

func main() {
	// acces X field using dot notation
	v1.X
}

```

- [Go Training - Structs](https://ultimate-go-copy.herokuapp.com/topics/go/language/struct_types/README.md)
- [Structs Exercise](./structs)

## Functions

A function can take zero or more arguments.

When two or more consecutive named function parameters share a type, you can omit the type from all but the last.

```go
func add(x int, y int) int {
	return x + y
}

func add(x, y int) int {
	return x + y
}
```

A function can return any number of results.

```go
func swap(x, y string) (string, string) {
	return y, x
}
```

Go's return values may be named. If so, they are treated as variables defined at the top of the function.

These names should be used to document the meaning of the return values.

A return statement without arguments returns the named return values. This is known as a "naked" return.

```go
func split(sum int) (x, y int) {
	x = sum * 4 / 9
	y = sum - x
	return
}
```

A defer statement defers the execution of a function until the surrounding function returns.

The deferred call's arguments are evaluated immediately, but the function call is not executed until the surrounding function returns.

```go
func main() {
	defer fmt.Println("world")

	fmt.Println("hello")
}
```

Functions are values too. They can be passed around just like other values.

Function values may be used as function arguments and return values.

```go
func compute(fn func(float64, float64) float64) float64 {
	return fn(3, 4)
}
```

**Error Handling**

Go code uses error values to indicate an abnormal state. For example, the os.Open function returns a non-nil error value when it fails to open a file.

```go
func Open(name string) (file *File, err error)
```

The error type is an interface type. An error variable represents any value that can describe itself as a string. Here is the interface's declaration:

```go
type error interface {
    Error() string
}
```

You can construct one of these values with the errors.New function. It takes a string that it converts to an errors.errorString and returns as an error value.

```go
func Sqrt(f float64) (float64, error) {
    if f < 0 {
        return 0, errors.New("math: square root of negative number")
    }
    // implementation
}
```

- [Go Training - Functions](https://ultimate-go-copy.herokuapp.com/topics/go/language/functions/README.md)
- [Functions Exercise](./functions)

## Pointers and Methods

**Pointers**

Go has pointers. A pointer holds the memory address of a value.

The & operator generates a pointer to its operand.

```go
i := 42
p = &i
```

The * operator denotes the pointer's underlying value.

```go
fmt.Println(*p) // read i through the pointer p
*p = 21         // set i through the pointer p
```

**Methods**

Go does not have classes.

A method is a function with a special receiver argument.

The receiver appears in its own argument list between the func keyword and the method name.

```go
type Vertex struct {
	X, Y float64
}

func (v Vertex) Abs() float64 {
	return math.Sqrt(v.X*v.X + v.Y*v.Y)
}

func main() {
	v := Vertex{3, 4}
	fmt.Println(v.Abs())
}
```

You can declare a method on non-struct types, too.

You can only declare a method with a receiver whose type is defined in the same package as the method. You cannot declare a method with a receiver whose type is defined in another package (which includes the built-in types such as int).

```go
type MyFloat float64

func (f MyFloat) Abs() float64 {
	if f < 0 {
		return float64(-f)
	}
	return float64(f)
}
```

You can declare methods with pointer receivers.

Methods with pointer receivers can modify the value to which the receiver points. 
Since methods often need to modify their receiver, pointer receivers are more common than value receivers.

```go
func (v *Vertex) Scale(f float64) {
	v.X = v.X * f
	v.Y = v.Y * f
}

func Scale(v *Vertex, f float64) {
	v.X = v.X * f
	v.Y = v.Y * f
}
```

- [Go Training - Pointers](https://ultimate-go-copy.herokuapp.com/topics/go/language/pointers/README.md)
- [Go Training - Methods](https://ultimate-go-copy.herokuapp.com/topics/go/language/methods/README.md)
- [Pointers Exercise](./pointers)

## Embedding 

Embedding is the process of including a type as a nameless parameter within another type, the exported parameters and methods defined on the embedded type are accessible through the embedding type.

Consider the following struct:

```go
type File struct {
    sync.Mutex
    rw io.ReadWriter
}
```

Then, File objects will directly have access to sync.Mutex methods:
```go
f := File{}
f.Lock()
```

- [Go Training - Embedding](https://ultimate-go-copy.herokuapp.com/topics/go/language/embedding/README.md)
- [Embedding Exercise](./embedding)

## Arrays and Slices 

An array's length is part of its type, so arrays cannot be resized.

```go
var a [10]int
```

A slice, on the other hand, is a dynamically-sized, flexible view into the elements of an array. In practice, slices are much more common than arrays.

A slice does not store any data, it just describes a section of an underlying array.

Slices can formed by specifying two indices, a low and high bound, separated by a colon:

```go
primes := [6]int{2, 3, 5, 7, 11, 13}
var s []int = primes[1:4]
```

Changing the elements of a slice modifies the corresponding elements of its underlying array.

A slice literal is like an array literal without the length.

```go
// Array literal
[3]bool{true, true, false}
```

And this creates the same array as above, then builds a slice that references it:

```go
// Slice literal
[]bool{true, true, false}
```

Slices can be created with the built-in make function; this is how you create dynamically-sized arrays.

The make function allocates a zeroed array and returns a slice that refers to that array:

```go
a := make([]int, 5)  // len(a)=5
```

- [Go Training - Slices](https://ultimate-go-copy.herokuapp.com/topics/go/language/slices/README.md)
- [Slices Exercise](./slices)

## Maps 

A map maps keys to values.

The make function returns a map of the given type, initialized and ready for use.

```go
var m map[string]int
m := make(map[string]int)

// create
m["Answer"] = 42

// read
v, ok := m["Answer"]

// update
m["Answer"] = 48

// delete 
delete(m, "Answer")
```

Map types are reference types, like pointers or slices, and so the value of m below is nil; it doesn't point to an initialized map.

```go
var m map[string]int
```

- [Go Training - Maps](https://ultimate-go-copy.herokuapp.com/topics/go/language/maps/README.md)
- [Maps Exercise](./maps)

## Interfaces

An interface type is defined as a set of method signatures.

A value of interface type can hold any value that implements those methods.

A type implements an interface by implementing its methods. There is no explicit declaration of intent, no "implements" keyword.

```go
type I interface {
	M()
}

type T struct {
	S string
}

// This method means type T implements the interface I,
// but we don't need to explicitly declare that it does so.
func (t T) M() {
	fmt.Println(t.S)
}
```

The interface type that specifies zero methods is known as the empty interface.

An empty interface may hold values of any type. (Every type implements at least zero methods.)

Empty interfaces are used by code that handles values of unknown type. For example, fmt.Print takes any number of arguments of type interface{}.

```go
func describe(i interface{}) {
	fmt.Printf("(%v, %T)\n", i, i)
}
```

- [Go Training - Interfaces](https://ultimate-go-copy.herokuapp.com/topics/go/language/interfaces/README.md)
- [Interfaces Exercise](./interfaces)

## Stretch

- [Software Design](https://ultimate-go-copy.herokuapp.com/topics/courses/go/design/README.md)
	- Go over Composition and Error Handling

## Design Philosophy - Interfaces and Composition

- Interfaces give programs structure.
- Interfaces encourage design by composition.
- Interfaces enable and enforce clean divisions between components.
- The standardization of interfaces can set clear and consistent expectations.
- Decoupling means reducing the dependencies between components and the types they use.
- This leads to correctness, quality and performance.
- Interfaces allow you to group concrete types by what they do.
- Don’t group types by a common DNA but by a common behavior.
- Everyone can work together when we focus on what we do and not who we are.
- Interfaces help your code decouple itself from change.
- You must do your best to understand what could change and use interfaces to decouple.
- Interfaces with more than one method have more than one reason to change.
- Uncertainty about change is not a license to guess but a directive to STOP and learn more.
- You must distinguish between code that:
- defends against fraud vs protects against accidents

**Validation**

Use an interface when:

* users of the API need to provide an implementation detail. 
* API’s have multiple implementations they need to maintain internally. 
* parts of the API that can change have been identified and require decoupling.

Don’t use an interface:

* for the sake of using an interface. 
* to generalize an algorithm. 
* when users can declare their own interfaces. 
* if it’s not clear how the interface makes the code better.

**Resources**

- [Methods, interfaces and Embedding](https://www.goinggo.net/2014/05/methods-interfaces-and-embedded-types.html) - William Kennedy
- [Composition with Go](https://www.goinggo.net/2015/09/composition-with-go.html) - William Kennedy
- [Reducing type hierarchies](https://www.goinggo.net/2016/10/reducing-type-hierarchies.html) - William Kennedy
- [Interface pollution in Go](https://medium.com/@rakyll/interface-pollution-in-go-7d58bccec275) - Burcu Dogan
- [Application Focused API Design](https://www.goinggo.net/2016/11/application-focused-api-design.html) - William Kennedy
- [Avoid interface pollution](https://www.goinggo.net/2016/10/avoid-interface-pollution.html) - William Kennedy
