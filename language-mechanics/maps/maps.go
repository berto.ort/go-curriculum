package maps

// Dictionary store definitions to words

// ErrNotFound means the definition could not be found for the given word
// ErrWordExists means you are trying to add a word that is already known
// ErrWordDoesNotExist occurs when trying to update a word not in the dictionary

// Search find a word in the dictionary

// Add inserts a word and definition into the dictionary

// Update changes the definition of a given word

// Delete removes a word from the dictionary
