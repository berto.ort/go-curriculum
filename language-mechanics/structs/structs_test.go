package structs

import "testing"

func TestStructs(t *testing.T) {
	name := "John"
	email := "john@mail.com"

	user := NewUser(name, email)

	if user.Name != name {
		t.Errorf("User name should be '%s' not '%s'", name, user.Name)
	}

	if user.Email != email {
		t.Errorf("User email should be '%s' not '%s'", email, user.Email)
	}

	rectangle := Rectangle{12.0, 6.0}

	got := Area(rectangle)
	want := 72.0

	if got != want {
		t.Errorf("got %.2f want %.2f", got, want)
	}
}
