package functions

import "testing"

func TestFunctions(t *testing.T) {
	assertCorrectMessage := func(t *testing.T, got, want interface{}) {
		t.Helper()
		if got != want {
			t.Errorf("got '%v' want '%v'", got, want)
		}
	}

	t.Run("checking if password is longer than 6", func(t *testing.T) {
		got := ValidPassword("cat")
		want := false
		assertCorrectMessage(t, got, want)
	})

	t.Run("doing division", func(t *testing.T) {
		got, err := Divide(15, 3)
		want := 5.0
		assertCorrectMessage(t, err, nil)
		assertCorrectMessage(t, got, want)

		_, err = Divide(3, 0)
		wantedError := "Can't divide by zero"
		assertCorrectMessage(t, err.Error(), wantedError)
	})
}
