# cnd-go-curriculum

## Resources

- [Ardanlab's Ultimate Go](https://ultimate-go-copy.herokuapp.com/)
- [Tour of Go](https://tour.golang.org/)
- [Learn Go with Tests](https://github.com/quii/learn-go-with-tests)
- [Gobyexample](https://gobyexample.com/)
- [Effective Go](https://golang.org/doc/effective_go.html)

## Curriculum

- [Part 1 - Language Mechanics](./language-mechanics/README.md)
- [Part 2 - Concurrency](./concurrency/README.md)
- [Part 3 - Testing](./testing/README.md)
- [Part 4 - Packages](./packages/README.md)
- [Part 5 - Persistence](./persistence/README.md)

## Usage

- [Install Go](https://github.com/quii/learn-go-with-tests/blob/master/install-go.md#install-go-set-up-environment-for-productivity)
- Run:
```
go get -d github.com/gschool/cnd-go-curriculum
cd $GOPATH/src/github.com/gschool/cnd-go-curriculum
```
- Read the markdown files and complete exercises by passing the tests. For example:
```
cd ./language-mechanics/types/
go test
```