## Part 4 - Packages


## Packages

Go programs are constructed by linking together packages. A package in turn is constructed from one or more source files that together declare constants, types, variables and functions belonging to the package and which are accessible in all files of the same package. 

Programs start running in package `main`.

By convention, the package name is the same as the last element of the import path. For instance, the "math/rand" package comprises files that begin with the statement package `rand`.

[Standard Library](https://golang.org/pkg/)

## Importing

An import declaration states that the source file containing the declaration depends on functionality of the imported package and enables access to exported identifiers of that package.

Assume we have compiled a package containing the package clause package math, which exports function Sin, and installed the compiled package in the file identified by "lib/math". This table illustrates how Sin is accessed in files that import the package after the various types of import declaration.

```go
Import declaration          Local name of Sin

import   "lib/math"         math.Sin
import m "lib/math"         m.Sin
import . "lib/math"         Sin
```

To import a package solely for its side-effects (initialization), use the blank identifier as explicit package name:

```go
import _ "lib/math"
```

## Exporting

A name is exported if it begins with a capital letter. For example, `Pizza` is an exported name, as is `Pi`, which is exported from the `math` package.

- [Go Training - Exporting](https://ultimate-go-copy.herokuapp.com/topics/go/language/exporting/README.md)

## Exercise

- Write a `findDictionaryWords` function that returns all words found in a dictionary meeting the criteria of a filter function. We can use this function to check if a word is valid and to find all possible words in a Boggle board.
    - The function takes a finder function and a path to a dictionary file ([words.txt](https://github.com/dwyl/english-words))
    - The finder function takes a word and returns true/false
    - The dictionary path is a string path to the `words.txt` file
    - Use the `os` package to open the file.
    - Remember to handle the error correctly and close the file connection
    - Use the `bufio` package to create a new scanner
- Organize the code inside the `boggle` directory into multiple packages
    - Rename or create new files or directories as you need
    - Change function names and add imports to correctly connect the files and pass the tests
- Execute the program

## Checkpoint

Use [this repo](https://github.com/gSchool/boggle) to have a good starting point to implement the function above.

## Design Philosophy - Packaging

Package Oriented Design allows a developer to identify where a package belongs inside a Go project and the design guidelines the package must respect. It defines what a Go project is and how a Go project is structured. Finally, it improves communication between team members and promotes clean package design and project architecture that is discussable.

[Learn More](https://ultimate-go-copy.herokuapp.com/topics/go/design/packaging/README.md)
