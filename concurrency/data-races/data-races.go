package goroutines

import "sync"

// WebsiteChecker checks a url, returning a bool
type WebsiteChecker func(string) bool

// Currently CheckWebsites has the potential of writing on results at the same time
// Use Mutexes to prevent a race condition
// Run `go test -race` to check your

// CheckWebsites takes a WebsiteChecker and a slice of urls and returns a map
// of urls to the result of checking each url with the WebsiteChecker function
func CheckWebsites(wc WebsiteChecker, urls []string) map[string]bool {
	results := make(map[string]bool)

	var wg sync.WaitGroup

	wg.Add(len(urls))

	for _, url := range urls {
		go func(url string) {
			results[url] = wc(url)
			wg.Done()
		}(url)
	}

	wg.Wait()
	return results
}
