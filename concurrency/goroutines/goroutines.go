package goroutines

// WebsiteChecker checks a url, returning a bool
type WebsiteChecker func(string) bool

// Currently CheckWebsites is checking urls one at a time
// Use go routines to check a slice of urls concurrently

// CheckWebsites takes a WebsiteChecker and a slice of urls and returns a map
// of urls to the result of checking each url with the WebsiteChecker function
func CheckWebsites(wc WebsiteChecker, urls []string) map[string]bool {
	results := make(map[string]bool)

	for _, url := range urls {
		results[url] = wc(url)
	}

	return results
}
