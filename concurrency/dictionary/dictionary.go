package dictionary

// Crate a function that takes a list of words and returns only valid words
// Use the following web API to check if a word is valid
// API: method GET, path /dictionary, query word
const dictionaryURL = "https://whispering-falls-21983.herokuapp.com/"
