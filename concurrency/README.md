# Part 2 - Concurrency

## Material

- [ ] [Goroutines](#goroutines) - [Goroutines Exercise](./goroutines) - [Channels Exercise](./channels) - [Wait Group Exercise](./channels)
- [ ] [Data Races](#data-races) - [Exercise](./data-races)
- [ ] [Design Philosophy - Concurrency](#design-philosophy---concurrency)

## Goroutines

A goroutine is a lightweight thread managed by the Go runtime.

Goroutines run in the same address space, so access to shared memory must be synchronized. 

```go
func say(s string) {
	for i := 0; i < 5; i++ {
		time.Sleep(100 * time.Millisecond)
		fmt.Println(s)
	}
}

func main() {
	go say("world")
	say("hello")
}
```

- [Go Training - Goroutines](https://ultimate-go-copy.herokuapp.com/topics/go/concurrency/goroutines/README.md)
- [Goroutines Exercise](./goroutines)

### Channels

Channels are a typed conduit through which you can send and receive values with the channel operator, <-.

The data flows in the direction of the arrow.

```go
ch <- v    // Send v to channel ch.
v := <-ch  // Receive from ch, and
           // assign value to v.
```

Like maps and slices, channels must be created before use:

```go
ch := make(chan int)
```

By default, sends and receives block until the other side is ready. This allows goroutines to synchronize without explicit locks or condition variables.

```go
func main() {
	c := make(chan struct{})
	go func() {
		time.Sleep(1 * time.Second)
		<-c
	}()
	start := time.Now()
	c <- struct{}{}
	elapsed := time.Since(start)
	fmt.Printf("Elapsed: %v\n", elapsed)
}
```

Channels can be buffered. Provide the buffer length as the second argument to make to initialize a buffered channel:

```go
ch := make(chan int, 10)
```

Sends to a buffered channel block only when the buffer is full. Receives block when the buffer is empty.

A sender can close a channel to indicate that no more values will be sent. 

```go
func fibonacci(n int, c chan int) {
	x, y := 0, 1
	for i := 0; i < n; i++ {
		c <- x
		x, y = y, x+y
	}
	close(c)
}

func main() {
	c := make(chan int, 10)
	go fibonacci(cap(c), c)
	for i := range c {
		fmt.Println(i)
	}
}
```

Receivers can test whether a channel has been closed by assigning a second parameter to the receive expression.

```go
v, ok := <-ch
```

The `select` statement lets a goroutine wait on multiple communication operations.

```go
func fibonacci(c, quit chan int) {
	x, y := 0, 1
	for {
		select {
		case c <- x:
			x, y = y, x+y
		case <-quit:
			fmt.Println("quit")
			return
		}
	}
}

func main() {
	c := make(chan int)
	quit := make(chan int)
	go func() {
		for i := 0; i < 10; i++ {
			fmt.Println(<-c)
		}
		quit <- 0
	}()
	fibonacci(c, quit)
}
```

- [Go Training - Channels](https://ultimate-go-copy.herokuapp.com/topics/go/concurrency/channels/README.md)
- [Channels Exercise](./channels)

### Wait Groups

A WaitGroup waits for a collection of goroutines to finish. 

```go
var wg sync.WaitGroup
```

The main goroutine calls Add to set the number of goroutines to wait for. 

```go
wg.Add(1)
```

Then each of the goroutines runs and calls Done when finished. 

```go
defer wg.Done()
```

At the same time, Wait can be used to block until all goroutines have finished.

```go
wg.Wait()
```

Example:

```go
func main() {
	var wg sync.WaitGroup
	var urls = []string{
		"http://www.golang.org/",
		"http://www.google.com/",
		"http://www.somestupidname.com/",
	}
	for _, url := range urls {
		// Increment the WaitGroup counter.
		wg.Add(1)
		// Launch a goroutine to fetch the URL.
		go func(url string) {
			// Decrement the counter when the goroutine completes.
			defer wg.Done()
			// Fetch the URL.
			http.Get(url)
		}(url)
	}
	// Wait for all HTTP fetches to complete.
	wg.Wait()
}
```

- [Waitgroups Exercise](./waitgroups)

## Data Races

A data race is when two or more goroutines attempt to read and write to the same resource at the same time.

```go
for _, url := range urls {
	go func(url string) {
		results[url] = wc(url)
	}(url)
}
```

Because we cannot control exactly when each goroutine writes to the results map, we are vulnerable to two goroutines writing to it at the same time

Go can help us to spot race conditions with its built in [race detector](https://blog.golang.org/race-detector). To enable this feature, run the tests with the race flag: `go test -race`.

We can solve this data race by coordinating our goroutines using channels or [Mutexes](https://gobyexample.com/mutexes)

- [Go Training - Data Races](https://ultimate-go-copy.herokuapp.com/topics/go/concurrency/data_race/README.md)
- [Data Races Exercise](./data-races)

## Stretch

- Use goroutines to make multiple HTTP requests. Follow the instructions in [dictionary.go](./dictionary/dictionary.go)
- Use the [Context](https://golang.org/pkg/context/) package to timeout goroutines
- Look into [Concurrency Patterns](https://ultimate-go-copy.herokuapp.com/topics/go/concurrency/patterns/README.md)

## Design Philosophy - Concurrency

### Concurrent Software Design

Concurrency is about managing multiple things at once. Like one person washing the dishes while they are also cooking dinner. You're making progress on both but you're only ever doing one of those things at the same time. Parallelism is about doing multiple things at once. Like one person cooking and placing dirty dishes in the sink, while another washes the dishes. They are happening at the same time.

Both you and the runtime have a responsibility in managing the concurrency of the application. You are responsible for managing these three things when writing concurrent software:

**Design Philosophy:**

* The application must startup and shutdown with integrity.
    * Know how and when every goroutine you create terminates.
    * All goroutines you create should terminate before main returns.
    * Applications should be capable of shutting down on demand, even under load, in a controlled way.
        * You want to stop accepting new requests and finish the requests you have (load shedding).
* Identify and monitor critical points of back pressure that can exist inside your application.
    * Channels, mutexes and atomic functions can create back pressure when goroutines are required to wait.
    * A little back pressure is good, it means there is a good balance of concerns.
    * A lot of back pressure is bad, it means things are imbalanced.
    * Back pressure that is imbalanced will cause:
        * Failures inside the software and across the entire platform.
        * Your application to collapse, implode or freeze.
    * Measuring back pressure is a way to measure the health of the application.
* Rate limit to prevent overwhelming back pressure inside your application.
    * Every system has a breaking point, you must know what it is for your application.
    * Applications should reject new requests as early as possible once they are overloaded.
        * Don’t take in more work than you can reasonably work on at a time.
        * Push back when you are at critical mass. Create your own external back pressure.
    * Use an external system for rate limiting when it is reasonable and practical.
* Use timeouts to release the back pressure inside your application.
    * No request or task is allowed to take forever.
    * Identify how long users are willing to wait.
    * Higher-level calls should tell lower-level calls how long they have to run.
    * At the top level, the user should decide how long they are willing to wait.
    * Use the `Context` package.
        * Functions that users wait for should take a `Context`.
            * These functions should select on <-ctx.Done() when they would otherwise block indefinitely.
        * Set a timeout on a `Context` only when you have good reason to expect that a function's execution has a real time limit.
        * Allow the upstream caller to decide when the `Context` should be canceled.
        * Cancel a `Context` whenever the user abandons or explicitly aborts a call.
* Architect applications to:
    * Identify problems when they are happening.
    * Stop the bleeding.
    * Return the system back to a normal state.