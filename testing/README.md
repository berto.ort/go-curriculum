## Part 3 - Testing

## Testing

Testing is built right into the go tools and the standard library.

```go
package math

import "testing"

func TestAverage(t *testing.T) {
  var v float64
  v = Average([]float64{1,2})
  if v != 1.5 {
    t.Error("Expected 1.5, got ", v)
  }
}
```

- [Go Training - Testing](https://ultimate-go-copy.herokuapp.com/topics/go/testing/tests/README.md)

## Benchmark

Go has support for testing the performance of your code.

```go
func BenchmarkItoa(b *testing.B) {
	number := 10
	var s string

	for i := 0; i < b.N; i++ {
		s = strconv.Itoa(number)
	}

	fs = s
}
```

Run the benchmark with `go test -bench=.`

Add a `-run` flag to skip tests

```go
go test -run=XXX -bench=.
```

Add a `-benchtime` to specify a benchmark time

```go
go test -run=XXX -bench=. -benchtime 3s
```

- [Go Training - Benchmark](https://ultimate-go-copy.herokuapp.com/topics/go/testing/tests/README.md)

## Exercise

To practice writing tests and packages, we will create the game of [Boggle](https://en.wikipedia.org/wiki/Boggle).

### Tasks

- Add a `boggle_test.go` in the current directory
- create a test for a random letter generator
    - We expect the function to take no inputs and output a random [letter character](https://golang.org/pkg/builtin/#rune)
- Run and fail the test
- Add a `boggle.go` file
- Write the function to pass the test
- Create a test for a board generator
    - We expect the board generator to take no inputs and output a boggle board
    - A boggle board consists of a [4x4 grid](https://gobyexample.com/slices) of random letters
- Fail, then pass the test
- Create benchmarks to test the performance of the functions

### Stretch

**Create a Boggle board grader**

- We expect our grader to take a list of words
- Given a board, the grader will mark which words appear in the board
- We expect the function to give a final score based on the words
- Suggestions
    - Create a function that takes a word and returns a score
    - Create a function that takes a board and a word, returns true/false if the word appears in the board
    - Create a function that takes a list of words and a board and returns a graded list of words

## Design Philosophy - Channels

Channels allow goroutines to communicate with each other through the use of signaling semantics. Channels accomplish this signaling through the use of sending/receiving data or by identifying state changes on individual channels. Don't architect software with the idea of channels being a queue, focus on signaling and the semantics that simplify the orchestration required.

**Language Mechanics:**

* Use channels to orchestrate and coordinate goroutines.
    * Focus on the signaling semantics and not the sharing of data.
    * Signaling with data or without data.
    * Question their use for synchronizing access to shared state.
        * _There are cases where channels can be simpler for this but initially question._
* Unbuffered channels:
    * Receive happens before the Send.
    * Benefit: 100% guarantee the signal has been received.
    * Cost: Unknown latency on when the signal will be received.
* Buffered channels:
    * Send happens before the Receive.
    * Benefit: Reduce blocking latency between signaling.
    * Cost: No guarantee when the signal has been received.
        * The larger the buffer, the less guarantee.
        * Buffer of 1 can give you one delayed send of guarantee.
* Closing channels:
    * Close happens before the Receive. (like Buffered)
    * Signaling without data.
    * Perfect for signaling cancellations and deadlines.
* NIL channels:
    * Send and Receive block.
    * Turn off signaling
    * Perfect for rate limiting or short-term stoppages.

**Design Philosophy:**

Depending on the problem you are solving, you may require different channel semantics. Depending on the semantics you need, different architectural choices must be taken.

* If any given Send on a channel `CAN` cause the sending goroutine to block:
    * Not allowed to use a Buffered channel larger than 1.
        * Buffers larger than 1 must have reason/measurements.
    * Must know what happens when the sending goroutine blocks.
* If any given Send on a channel `WON'T` cause the sending goroutine to block:
    * You have the exact number of buffers for each send.
        * Fan Out pattern
    * You have the buffer measured for max capacity.
        * Drop pattern
* Less is more with buffers.
    * Don’t think about performance when thinking about buffers.
    * Buffers can help to reduce blocking latency between signaling.
        * Reducing blocking latency towards zero does not necessarily mean better throughput.
        * If a buffer of one is giving you good enough throughput then keep it.
        * Question buffers that are larger than one and measure for size.
        * Find the smallest buffer possible that provides good enough throughput.